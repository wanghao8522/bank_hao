package com.bank.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.bank.Utils.DBUtils;
import com.bank.vo.UserLogin;

public class UserLoginDao {
	
	
	
	public UserLogin login(String account_no,String password){
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		UserLogin ul = null;
		
		
		
		try {
			
			
			conn = DBUtils.getConnection();
			
			String sql="select * from b_user_login where account_no=? and password=?";
			
			pstmt = DBUtils.getPareparedStatement(conn, sql);
			
			
			pstmt.setString(1,account_no);
			
			pstmt.setString(2, password);
			
			
			rs = pstmt.executeQuery();
			
			while(rs.next()){
				
				String an= rs.getString("account_no");
				String pwd = rs.getString("password");
				
				ul = new UserLogin();
				ul.setAccount_no(an);
				ul.setPassword(pwd);
				
				
				
			}
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			
			DBUtils.close(conn, pstmt, rs);
		}
		
		
		return ul;
		
		
	}

}
