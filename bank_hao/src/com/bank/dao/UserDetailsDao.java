package com.bank.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bank.Utils.DBUtils;
import com.bank.vo.UserDetails;

public class UserDetailsDao {
	
	
	public static void main(String[] args){
		

//		getDetails();
	}
	
	
	
	public  List<UserDetails> getDetails(){
		
		
		Connection conn = null;
		
		PreparedStatement pstmt =null;
		
		ResultSet rs = null;
		
		List<UserDetails> userDetails = new ArrayList();
		
		

		
		try {
			
			conn = DBUtils.getConnection();
			
			String sql = "select * from  b_user_details";
			
			pstmt = DBUtils.getPareparedStatement(conn, sql);
			
			rs = pstmt.executeQuery();
			
			while(rs.next()){
				
				UserDetails ud = new UserDetails();
				
				ud.setId(rs.getInt("id"));
				ud.setAccount_no(rs.getInt("account_no"));
				ud.setAccount_type(rs.getString("account_type"));
				ud.setMoney_type(rs.getString("money_type"));
				ud.setNick_name(rs.getString("nick_name"));
				ud.setAccount_name(rs.getString("account_name"));
				
				
				userDetails.add(ud);
				
				//System.out.println(rs.getInt("id")+"\t"+rs.getString("account_no")+"\t"+rs.getString("account_type")+"\t"+rs.getString("money_type"));
			}
			
			
			
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			
			DBUtils.close(conn, pstmt, rs);
		}
		
		
		return userDetails;
	}

}
