package com.bank.Utils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class DBUtils {
	
	  private static String dbtype;
	  private static String driver;
	  private static String url;
	  private static String user;
	  private static String password;
	 
   
	
	
	
	
	static{
		
		
		   Properties p = new Properties();
		   
		   try {
			p.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("DBConfig.properties"));
			
			
			dbtype =	p.getProperty("dbtype");
			driver =	p.getProperty(dbtype+"Driver");
			url =	p.getProperty(dbtype+"Url");
			user =	p.getProperty(dbtype+"User");
			password =	p.getProperty(dbtype+"Pwd");
			
			Class.forName(driver);
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		   
		   
		
		
		
	}
	
	
public static void main (String[] args){
		
		System.out.println(dbtype);
		System.out.println(driver);
		
		System.out.println(url);
		System.out.println(user);
		System.out.println(password);
	}


 public static Connection getConnection(){
	   
	   
	      Connection conn = null;
	      
	      try {
			conn = DriverManager.getConnection(url, user, password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   
	   
	       return conn;
	   
	   
 }
 
 public static Statement getStatement(Connection conn){
	   
	   Statement stmt = null;
	   
	   try {
		stmt = conn.createStatement();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	   
	   
	   
	   return stmt;
	   
 }
 
 
 public static PreparedStatement getPareparedStatement(Connection conn, String sql){
	   
	   
	   PreparedStatement pstmt = null;
	   
	   try {
		pstmt = conn.prepareStatement(sql);
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	   
	   
	   return pstmt;
 }
 
 
 public static void close(Connection conn, PreparedStatement pstmt,ResultSet rs){
	   
	   if( conn !=null){
		   
		   try {
			     conn.close();
			     
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		   
		   
	   }
	   
	   
	   if(pstmt !=null){
		   
		   
		   try {
				pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	   }
	   
	   if(rs != null){
		   
		   try {
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   }
 }


}
