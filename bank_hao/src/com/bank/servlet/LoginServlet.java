package com.bank.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bank.dao.UserLoginDao;
import com.bank.vo.UserLogin;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		
		request.setCharacterEncoding("utf-8");
		String account_no = request.getParameter("account_no");
		String password = request.getParameter("password");
		
		System.out.println(account_no);
		System.out.println(password);
		
		UserLoginDao uld= new UserLoginDao();
		
	    UserLogin login = uld.login(account_no, password);
	    
	    if( login!=null){
	    	
	    	request.getRequestDispatcher("/UserDetailsServlet").forward(request, response);
	    	 System.out.println("success");
	    }else{
	    	
	    	request.getRequestDispatcher("login.jsp").forward(request, response);
	    	 System.out.println("fail");
	    }
		
	}

}
