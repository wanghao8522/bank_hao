/*
MySQL Data Transfer
Source Host: localhost
Source Database: bank_hao
Target Host: localhost
Target Database: bank_hao
Date: 6/3/2016 1:02:36 PM
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for b_user_details
-- ----------------------------
CREATE TABLE `b_user_details` (
  `id` int(10) NOT NULL DEFAULT '0',
  `account_no` int(255) DEFAULT NULL,
  `account_type` varchar(255) DEFAULT NULL,
  `money_type` varchar(255) DEFAULT NULL,
  `nick_name` varchar(255) DEFAULT NULL,
  `account_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for b_user_login
-- ----------------------------
CREATE TABLE `b_user_login` (
  `id` int(10) NOT NULL DEFAULT '0',
  `account_no` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `b_user_details` VALUES ('1', '1244002194', '基本存款账户', '纽币', 'Albert', '尧天有限公司');
INSERT INTO `b_user_login` VALUES ('1', '1244002194', 'albert', '123456');
