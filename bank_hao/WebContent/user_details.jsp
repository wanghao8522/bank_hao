<%@ page language="java" import="java.util.*" pageEncoding="GBK"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<title>Liana企业会员中心</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="css/custom.css"/>
	<link rel="stylesheet" type="text/css" href="css/css.css"/>
	<link rel="stylesheet" type="text/css" href="css/index.css"/>
	<link rel="stylesheet" type="text/css" href="css/demo.css"/>
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/nav.js"></script>
</head>
<body class="container">
	<!-- header start -->
	<div class='head'>
		<!-- title start -->
		<div class="title">
			<div class="title-r">
		  		<ul>
					<li>欢迎录入授权管理</li>
					<li><a href="#">企业网上营业厅</a></li>
					<li style="background:none;"><a href="login.html" >安全退出</a></li>				
		  		</ul>
			</div>
		</div>
		<!-- title end -->

		<!-- logo start -->
		<div class="logo">
			<h3 class="">Liana企业会员中心</h3>
		</div>
		<!-- logo end -->

		<!-- nav start -->
		<div>
			<div class="head-v3">
				<div class="navigation-up">
					<div class="navigation-inner">
						<div class="navigation-v3">
							<ul>
								<li class="" _t_nav="home">
									<h2>
										<a href="index.html">首页</a>
									</h2>
								</li>
								<li class="" _t_nav="product">
									<h2>
										<a href="">应用中心</a>
									</h2>
								</li>
								<li class="" _t_nav="wechat">
									<h2>
										<a href="">授权中心</a>
									</h2>
								</li>
								<li class="" _t_nav="solution">
									<h2>
										<a href="">安全中心</a>
									</h2>
								</li>
								<li class="" _t_nav="company">
									<h2>
										<a href="">企业中心</a>
									</h2>
								</li>
							</ul>
						</div>
					</div>
			</div>
			<div class="navigation-down">
				<div id="company" class="nav-down-menu menu-1" style="display: none;" _t_nav="company">
					<div class="navigation-down-inner">
						<dl style="margin-left: 100px;">
							<dt>账户查询</dt>
							<dd>
								<a href="账户余额查询.html">账户余额查询</a>
							</dd>
							<dd>
								<a href="账户明细查询.html">账户明细查询</a>
							</dd>
							<dd>
								<a href="网银流水查询.html">网银流水查询</a>
							</dd>
						</dl>
						<dl>
							<dt>贷款还款</dt>
							<dd>
								<a href="贷款查询.html">贷款查询</a>
							</dd>
							<dd>
								<a href="还款明细.html">还款明细</a>
							</dd>
							<dd>
								<a href="设置贷款还款计划.html">设置贷款还款计划</a>
							</dd>
						</dl>
						<dl>
							<dt>行内相关</dt>
							<dd>
								<a href="行内转账.html">行内转账</a>
							</dd>
							<dd>
								<a href="行内批量转账.html">行内批量转账</a>
							</dd>
							<dd>
								<a href="行内批量转账查询.html">行内批量转账查询</a>
							</dd>
						</dl>
						<dl>
							<dt>行外相关</dt>
							<dd>
								<a href="跨行转账.html">跨行转账</a>
							</dd>
							<dd>
								<a href="跨行批量转账.html">跨行批量转账</a>
							</dd>
							<dd>
								<a href="跨行批量转账查询.html">跨行批量转账查询</a>
							</dd>
						</dl>
						<dl>
							<dt>预约转账</dt>
							<dd>
								<a href="预约转账计划提交.html">预约转账计划提交</a>
							</dd>
							<dd>
								<a href="预约转账计划取消.html">预约转账计划取消</a>
							</dd>
						</dl>
						<dl>
							<dt>定期相关</dt>
							<dd>
								<a href="定期存款.html">定期存款</a>
							</dd>
							<dd>
								<a href="开立定期存款.html">开立定期存款</a>
							</dd>
						</dl>
					</div>
				</div>
			</div>
			
		</div>
		<!-- nav end -->
	</div>
	<!-- header end -->

	<!-- main start -->
	<div class="main">
		<!-- mian-l start -->
		<div class="main-l">
			<div class="m-l-site account">
					<h5>帐号列表</h5>
					<c:forEach items="${list}" var="details">
					<p><span>帐号：　${details.account_no }</span><span>币种:${details.money_type }</span><span>类型：　${details.account_type}</span></p>
					<p><span>别名：${details.nick_name}</span><span>账户名：${details.account_name }　</span></p>
					
					</c:forEach>
			</div>

			<div class="m-l-site function">
				<h5>常用功能</h5>
				<a href="">设置</a>
				<ul>
					<li><p>1</p><span>11</span></li>
					<li><p>1</p><span>22</span></li>
					<li><p>1</p><span>33</span></li>
					<li><p>1</p><span>44</span></li>
					<li><p>1</p><span>55</span></li>
				</ul>
			</div>

			<div class="m-l-site journal">
				<h5>操作员日志</h5>
				<a href="">更多</a>
				<ul>
					<li>
						<p><span>交易名称：　用户登录</span><span class='span'> 时间：　2015/10/13</span></p>
						<p><span>企业会员号：　CB1122114</span><span>用户号：　100001</span></p>
					</li>
				</ul>
				<ul>
					<li>
						<p><span>交易名称：　用户登录</span><span class='span'> 时间：　2015/10/13</span></p>
						<p><span>企业会员号：　CB1122114</span><span>用户号：　100001</span></p>
					</li>
				</ul>
			</div>
		</div>
		<!-- main-l end -->

		<!-- main-r start-->
		<div class="main-r">
			<div class="m-r-first">
				<h5>企业信息</h5>
				<h6>宇信易城科技有限公司</h6>
				<p>欢迎您，录入授权管理</p>
				<p class='s'>上次登录：2015/10/13/15:10</p>
			</div>

			<div class="m-r-second">
				<h5>通知</h5><span>更多</span>
				<ul>
					<li>通知1：(行内转账)序号为CB1122114的指令在授权时被拒绝</li>
				</ul>
			</div>

			<div class="m-r-thrid">
				<h5>银行公告</h5><span>更多</span>
				<ul>
					<li>利率公告：人行将近期调整利率，请关注</li>
					<li>新闻公告：关于近期国家调整利率</li>
					<li>外汇汇率转公告：外汇汇率</li>
					<li>银行利率：银行利率</li>
				</ul>
			</div>

			<div class="m-r-fourth">
				<h5>待授权</h5>
				<ul>
					<li>你有九笔待授权</li>
				</ul>
			</div>
		</div>
		<!-- main-r end-->
	</div>
	<!-- main end -->

	<!-- footer start -->
	<div class='footer'>
		<div class="footer-c">
           <ul>
				<li><a href="#">关于Liana</a></li>
				<li><a href="#">网站声明</a></li>
				<li><a href="#" >加入收藏</a></li>	
				<li style="background:none;"><a href="#">友情链接</a></li>				
		  	</ul>          
        </div>
        <div class="footer-copy">
        	<p>版权所有：Liana银行</p>
        </div>
	</div>
	<!-- footer end -->
</body>
</html>
