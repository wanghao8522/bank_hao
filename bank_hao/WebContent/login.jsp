<%@ page language="java" import="java.util.*" pageEncoding="GBK"%>
   
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<title>Liana企业会员中心</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="css/custom.css"/>
	<link rel="stylesheet" type="text/css" href="css/css.css"/>
	<link rel="stylesheet" type="text/css" href="css/demo.css"/>
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/nav.js"></script>
</head>
<body class="container">
	<!-- header start -->
	<div class='head'>
		<!-- title start -->
		<div class="title">
			<div class="title-r">
		  		<ul>
					<li>欢迎录入授权管理</li>
					<li><a href="#">企业网上营业厅</a></li>
					<li style="background:none;" class="hidden"><a href="#" >安全退出</a></li>					
		  		</ul>
			</div>
		</div>
		<!-- title end -->

		<!-- logo start -->
		<div class="logo">
			<h3 class="">Liana企业会员中心</h3>
		</div>
		<!-- logo end -->

		<!-- nav start -->
		<div>
			<div class="head-v3">
				<div class="navigation-up">
					<div class="navigation-inner">
						<div class="navigation-v3">
							<ul>
								<li class="" _t_nav="home">
									<h2>
										<a href="index.html">首页</a>
									</h2>
								</li>
								<li class="" _t_nav="product">
									<h2>
										<a href="">应用中心</a>
									</h2>
								</li>
								<li class="" _t_nav="wechat">
									<h2>
										<a href="">授权中心</a>
									</h2>
								</li>
								<li class="" _t_nav="solution">
									<h2>
										<a href="">安全中心</a>
									</h2>
								</li>
								<li class="" _t_nav="company">
									<h2>
										<a href="">企业中心</a>
									</h2>
								</li>
							</ul>
						</div>
					</div>
			</div>
			<div class="navigation-down">
				<div id="company" class="nav-down-menu menu-1" style="display: none;" _t_nav="company">
					<div class="navigation-down-inner">
						<dl style="margin-left: 100px;">
							<dt>账户查询</dt>
							<dd>
								<a href="账户余额查询.html">账户余额查询</a>
							</dd>
							<dd>
								<a href="账户明细查询.html">账户明细查询</a>
							</dd>
							<dd>
								<a href="网银流水查询.html">网银流水查询</a>
							</dd>
						</dl>
						<dl>
							<dt>贷款还款</dt>
							<dd>
								<a href="贷款查询.html">贷款查询</a>
							</dd>
							<dd>
								<a href="还款明细.html">还款明细</a>
							</dd>
							<dd>
								<a href="设置贷款还款计划.html">设置贷款还款计划</a>
							</dd>
						</dl>
						<dl>
							<dt>行内相关</dt>
							<dd>
								<a href="行内转账.html">行内转账</a>
							</dd>
							<dd>
								<a href="行内批量转账.html">行内批量转账</a>
							</dd>
							<dd>
								<a href="行内批量转账查询.html">行内批量转账查询</a>
							</dd>
						</dl>
						<dl>
							<dt>行外相关</dt>
							<dd>
								<a href="跨行转账.html">跨行转账</a>
							</dd>
							<dd>
								<a href="跨行批量转账.html">跨行批量转账</a>
							</dd>
							<dd>
								<a href="跨行批量转账查询.html">跨行批量转账查询</a>
							</dd>
						</dl>
						<dl>
							<dt>预约转账</dt>
							<dd>
								<a href="预约转账计划提交.html">预约转账计划提交</a>
							</dd>
							<dd>
								<a href="预约转账计划取消.html">预约转账计划取消</a>
							</dd>
						</dl>
						<dl>
							<dt>定期相关</dt>
							<dd>
								<a href="定期存款.html">定期存款</a>
							</dd>
							<dd>
								<a href="开立定期存款.html">开立定期存款</a>
							</dd>
						</dl>
					</div>
				</div>
			</div>
			
		</div>
		<!-- nav end -->
	</div>
		
	<!-- header end -->

	<!-- main start -->
	<div class="main">
		<div  class="contenttitle" align="center">
			登录
		</div>
		<div>
			<form class="form-horizontal" action="LoginServlet" method="post">
			  <div class="form-group">
			    <label for="inputEmail3"  class="col-sm-4 control-label">登录帐号</label>
			    <div class="col-sm-4">
			      <input type="text" name="account_no"class="form-control" id="inputEmail3" placeholder="请输入登录帐号">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputPassword3" class="col-sm-4 control-label">密码</label>
			    <div class="col-sm-4">
			      <input type="password"name="password" class="form-control" id="inputPassword3" placeholder="请输入密码">
			    </div>
			  </div>
			  <div class="form-group">
			    <div class="col-sm-offset-4 col-sm-4">
			      <div class="checkbox">
			        <label>
			          <input type="checkbox">记住密码
			        </label>
			      </div>
			    </div>
			  </div>
			  <div class="form-group">
			    <div class="col-sm-offset-5 col-sm-2">
			      <button type="submit" class="btn btn-default" >登录</button>
			    </div>
			  </div>
			</form>
		</div>
	</div>
	<!-- main end -->

	<!-- footer start -->
	<div class='footer'>
		<div class="footer-c">
           <ul>
				<li><a href="#">关于Liana</a></li>
				<li><a href="#">网站声明</a></li>
				<li><a href="#" >加入收藏</a></li>	
				<li style="background:none;"><a href="#">友情链接</a></li>				
		  	</ul>          
        </div>
        <div class="footer-copy">
        	<p>版权所有：Liana银行</p>
        </div>
	</div>
	<!-- footer end -->
</body>
</html>
